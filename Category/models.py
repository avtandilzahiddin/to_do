from django.conf import settings
from django.db import models

class Category(models.Model):
    title = models.CharField(max_length=25)
    description = models.TextField(verbose_name='Описание')
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE, 'owners_category')

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ('title',)

    def __str__(self):
        return self.title
