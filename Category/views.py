from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from Category.models import Category
from Category.permissions import IsCategoryOwner
from Category.serializers import CategorySerializer


class CategoryView(ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    lookup_field = 'pk'
    permission_classes = (IsCategoryOwner, )

    def list(self, request):
        owner = request.user
        queryset = Category.objects.filter(owner=owner)
        serializer = CategorySerializer(queryset, many=True)
        return Response(serializer.data)


