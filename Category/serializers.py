from rest_framework import serializers

from Category.models import Category

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        read_only_fields = ('owner', )


    def create(self, validated_data):
        owner = self.context.get('request').user
        category = Category.objects.create(owner=owner, **validated_data)
        return category