from django.conf import settings
from django.db import models


class Task(models.Model):
    title = models.CharField(max_length=25)
    description = models.TextField(verbose_name='Описание')
    deadline = models.DateField(verbose_name='Дедлайн')
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE, 'task_owner')
    category = models.ForeignKey('Category.Category', models.CASCADE, 'task_category')
    date = models.DateTimeField('Дата создания', auto_now_add=True)

    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'
        # ordering = ('-date', )

    def __str__(self):
        return self.title
