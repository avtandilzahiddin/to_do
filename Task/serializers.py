from rest_framework import serializers
from Task.models import Task


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'
        read_only_fields = ('owner', )

    def create(self, validated_data):
        owner = self.context.get('request').user
        task = Task.objects.create(owner=owner, **validated_data)
        return task