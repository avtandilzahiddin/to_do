from django.shortcuts import render
from rest_framework import status
from rest_framework.viewsets import ModelViewSet

from Category.models import Category
from Task.models import Task
from Task.permissions import IsTaskOwnerOrReadOnly
from Task.serializers import TaskSerializer
from rest_framework.response import Response


class TaskView(ModelViewSet):
    serializer_class = TaskSerializer
    queryset = Task.objects.all()
    lookup_field = 'pk'
    permission_classes = (IsTaskOwnerOrReadOnly, )

    def create(self, request, *args, **kwargs):
        category = Category.objects.get(id=request.data.get('category'))
        if request.user != category.owner:
            return Response('У вас нет такой категории! ¯|_(ツ)_/¯ ', status=status.HTTP_400_BAD_REQUEST)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def list(self, request):
        owner = request.user
        queryset = Task.objects.filter(owner=owner)
        serializer = TaskSerializer(queryset, many=True)
        return Response(serializer.data)