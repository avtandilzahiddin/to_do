from rest_framework.response import Response
from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet

from User.models import User
from User.permissions import IsUserOrReadOnly
from User.serializers import UserSerializer


class UserView(ModelViewSet):

    serializer_class = UserSerializer
    queryset = User.objects.all()
    lookup_field = 'pk'
    permission_classes = (IsUserOrReadOnly, )
